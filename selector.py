import math
import random
import uuid
import os
from typing import Dict


# Get the number of sweep leagues to generate
league_files = os.listdir("players")
for league in league_files:
    tier_lists = []
    draw_lists = []
    player_list = []
    teams = []
    player_picks: Dict[str, list] = dict()
    csv_detail = ""

    # Get list of players in sweep
    with open(f'players/{league}', 'r') as player_file:
        player_list.extend(player_file.readlines())

    player_count = len(player_list)

    # Compile Tiers
    tier_files = os.listdir("teams")
    for file in tier_files:
        with open(f"teams/{file}", 'r') as tier_file:
            tier_list = tier_file.readlines()
            tier_lists.append(tier_list)

    # Compile Draw matches
    for tier_list in tier_lists:
        length = len(tier_list)
        multiple = math.ceil(player_count/length)
        draw_list = []
        for counter in range(multiple):
            draw_list.extend(tier_list)
        draw_lists.append(draw_list)

    # Assign picks
    for player in player_list:
        teams = []
        rnd = random.Random(hash(uuid.UUID))
        for tier in draw_lists:
            pick = rnd.randint(0, len(tier) - 1)
            teams.append(tier.pop(pick))
        player_picks.update({player: teams})

    # TODO: Check for randomly convergent choices

    # Compile user lines for the CSV
    for player_assignment in player_picks:
        csv_line = "{player_name}".format(player_name=player_assignment.rstrip("\n"))
        for player_team in player_picks[player_assignment]:
            team_out = player_team.rstrip("\n")
            csv_line += ",{player_team}".format(player_team=team_out)
        csv_detail += f"{csv_line}\n"

    with open(f"data/outputs/{league}.csv", 'w', newline='') as csvfile:
        writer = csvfile.write(csv_detail)
