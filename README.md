# Pool Sweeper

An automated system for controlling customised sweeps for pool based sporting events. 

---

## v 0.1 : Team Assignment Script

A customisable team selector for sweep competitions.

### Usage

Place correctly formatted files in the relevant directories and run the script. Retrieve the assignments from the Outputs folder which will be in CSV format. It's that simple right now as it was coded and tested in a lunch hour.

### Directories
#### Players
Lists of players can be added to the players directory as an ASCII text file, one player per line. Each list is assigned teams separately and thus can be used as divisions for a larger sweep competition. 

#### Tiers
The tiers directory is used for ASCII text file containing the names of the teams to be assigned. One team is selected from each list for each player. If there are multiple lists, multiple assignments will be made per player.


